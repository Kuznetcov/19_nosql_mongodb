<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/style2.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">
	<c:set var="object" value="${place}" />

		<div class="b-page__line">
			<div class="b-menu">
				<a href="../places" class='b-link b-link_menu'>HOME</a>
				<a href="search" class='b-link b-link_menu'>SEARCH</a> <a
					href="agreement" class='b-link b-link_menu'>SERVICES</a> <a
					href="#" class='b-link b-link_menu'>CUSTOMERS</a> <a href="#"
					class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass">
				
				<div class ="content-text">
				
        		<table>
	        		<c:forEach var="comment" items="${object.getComments()}">
	        		
	            		<c:catch>
	            		<tr>
	            			<th>CommentID: ${comment.getId()} <br>Username: ${comment.getUsername()} <br>Rating: ${comment.getRating()} <br></th>
	            			<td>${comment.getText()}</td>
	            		</tr>
	            		</c:catch>
	        		</c:forEach>
				</table>
				
				<sec:authorize access="isAuthenticated()">
			   		 <sec:authentication property="principal.firstname" var="username" />
				</sec:authorize>
				
				<form:form action="" class="form-style-9" method="post" commandName="userComment">
				
				
				Add comment:<br>
				Your mark:
				<form:select path="rating" items="${marks}" /><br>
 				<form:input type="text" name="comment" class="field-style" placeholder="Comment" path="text"></form:input>
 				<form:input type="hidden" name="username" path="username" value="${username}"></form:input>
				<input type="submit" value="Send"></input>
				</form:form>
				
				</div>
				</main>
				<!-- .content -->

			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->

	</div>
	<!-- .wrapper -->

</body>
</html>