package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dal.ModelComposer;
import com.data.ParkingPlace;

@Controller
@RequestMapping(value = "/inside")
public class MainPage {

  @Autowired
  private ModelComposer modelComposer;

  @RequestMapping(value = "/neverDirectUsedPageToShowParkingPlaces", method = RequestMethod.GET)
  public String showParkingPlaces(ModelMap model,
      @CookieValue(value = "friendlyusername", defaultValue = "%username") String username) {

    List<ParkingPlace> places = modelComposer.getParkingPlaces();
    model.addAttribute("textA", places);
    model.addAttribute("username", username);
    return "places";
  }  

  @RequestMapping(value = "/places", method = RequestMethod.GET)
  public String sessionCheck(ModelMap model, HttpServletRequest request) {

      return "forward:neverDirectUsedPageToShowParkingPlaces";
  }
}
