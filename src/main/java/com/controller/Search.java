package com.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dal.ModelComposer;
import com.data.Comment;
import com.data.ParkingPlaceMongo;

@Controller
@RequestMapping("/inside")

public class Search {

static Logger log = LoggerFactory.getLogger(Login.class);
  
  @Autowired
  private ModelComposer modelComposer;
  
  
  @RequestMapping(value="/search",method = RequestMethod.GET)
  public String doGet(ModelMap model)
      throws ServletException, IOException {
    return "search";
  }
  
  @RequestMapping(value="/search",method = RequestMethod.POST)
  public String doPost(ModelMap model,@RequestParam("text") String text)
      throws ServletException, IOException {
    if (!text.isEmpty()){
      List<Comment> comments = modelComposer.searchCommentByText(text);
      model.addAttribute("comments",comments);
    }
    return "search";
  }
  
}
