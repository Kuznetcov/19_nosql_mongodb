package com.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dal.ModelComposer;
import com.data.Comment;
import com.data.ParkingPlaceMongo;

@Controller
@RequestMapping("/inside")

public class ParkingPlaceInfo {

static Logger log = LoggerFactory.getLogger(Login.class);
  
  @Autowired
  private ModelComposer modelComposer;
  
  
  @RequestMapping(value="/places/{id}",method = RequestMethod.GET)
  public String doGet(ModelMap model, @PathVariable("id") String id)
      throws ServletException, IOException {
    Comment comment = new Comment();
    ParkingPlaceMongo place = modelComposer.getParkingPlaceInfo(Integer.valueOf(id));
    System.out.println(place);
    
    Map<String,String> marks = new LinkedHashMap<String,String>();
    marks.put("3", "3");
    marks.put("2", "2");
    marks.put("1", "1");
    model.addAttribute("marks",marks);
    
    model.addAttribute("place",place);
    model.put("userComment", comment);
    return "placeinfo";
  }
  
  @RequestMapping(value="/places/{id}",method = RequestMethod.POST)
  public String doPost(ModelMap model,@ModelAttribute("userComment") Comment comment, @PathVariable("id") String id)
      throws ServletException, IOException {
    
    System.out.println(comment);
    modelComposer.addComment(comment, id);
    return "redirect:/inside/places/"+id;
  }
  
}
