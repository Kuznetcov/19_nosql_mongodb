package com.controller.data;

import java.io.Serializable;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@Component
public class SessionDataBean implements Serializable {

  private static final long serialVersionUID = -8889115822669749895L;
  String name;
  String login;
  String password;
  String sessionId;

  public SessionDataBean(String name, String login, String password, String sessionId) {
    super();
    this.name = name;
    this.login = login;
    this.password = password;
    this.sessionId = sessionId;
  }

  public SessionDataBean() {
    super();
  }

  public synchronized String getName() {
    return name;
  }

  public synchronized void setName(String name) {
    this.name = name;
  }

  public synchronized String getLogin() {
    return login;
  }

  public synchronized void setLogin(String login) {
    this.login = login;
  }

  public synchronized String getPassword() {
    return password;
  }

  public synchronized void setPassword(String password) {
    this.password = password;
  }

  public synchronized String getSessionId() {
    return sessionId;
  }

  public synchronized void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

}
