package com.data;

import java.sql.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(name="PlaceEvent", propOrder={"eventID", "placeID", "date", "event"})
@XmlAccessorType(XmlAccessType.FIELD)
public class PlaceEvent {

  @XmlElement(name="eventID")
  public Integer eventID;
  @XmlElement(name="placeID")
  public Integer placeID;
  @XmlElement(name="date")
  public String date;
  @XmlElement(name="event")
  public Event event;
  
  public synchronized Integer getEventID() {
    return eventID;
  }
  public synchronized void setEventID(Integer eventID) {
    this.eventID = eventID;
  }
  public synchronized Integer getPlaceID() {
    return placeID;
  }
  public synchronized void setPlaceID(Integer placeID) {
    this.placeID = placeID;
  }
  public synchronized String getDate() {
    return date;
  }
  public synchronized void setDate(Date date) {
    System.out.println(date.toLocalDate());
    this.date = date.toLocalDate().toString();;
  }
  public synchronized Event getEvent() {
    return event;
  }
  public synchronized void setEvent(Event event) {
    this.event = event;
  }
  
  
  
}
