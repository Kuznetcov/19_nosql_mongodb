package com.data;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(name="Car", propOrder={"model", "ownerID", "number"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Car implements Serializable {


  /**
   * 
   */
  private static final long serialVersionUID = -8151600985366758441L;

  @XmlElement(name="model")
  public String model;

  @XmlElement(name="ownerID")
  public Integer ownerID;

  @XmlElement(name="number")
  public Integer number;

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public Integer getOwnerID() {
    return ownerID;
  }

  public void setOwnerID(Integer ownerID) {
    this.ownerID = ownerID;
  }

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }


}
