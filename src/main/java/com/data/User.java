package com.data;

import java.sql.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

  @JsonProperty("name")
  public String username;
  public String login;
  public String password;
  @JsonProperty("birthDate")
  public Date birthdate;
  public String email;
  public Set<Roles> roles;
  public User(String username, String login, String password, Date birthdate, String email,
      Set<Roles> roles) {
    super();
    this.username = username;
    this.login = login;
    this.password = password;
    this.birthdate = birthdate;
    this.email = email;
    this.roles = roles;
  }
  public User(String username, String login, String password) {
    super();
    this.username = username;
    this.login = login;
    this.password = password;
  }
  public User() {
    super();
  }
  public synchronized String getUsername() {
    return username;
  }
  public synchronized void setUsername(String username) {
    this.username = username;
  }
  public synchronized String getLogin() {
    return login;
  }
  public synchronized void setLogin(String login) {
    this.login = login;
  }
  public synchronized String getPassword() {
    return password;
  }
  public synchronized void setPassword(String password) {
    this.password = password;
  }
  public synchronized Date getBirthdate() {
    return birthdate;
  }
  public synchronized void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }
  public synchronized String getEmail() {
    return email;
  }
  public synchronized void setEmail(String email) {
    this.email = email;
  }
  public synchronized Set<Roles> getRoles() {
    return roles;
  }
  public synchronized void setRoles(Set<Roles> roles) {
    this.roles = roles;
  }
  @Override
  public String toString() {
    return "User [username=" + username + ", login=" + login + ", password=" + password
        + ", birthdate=" + birthdate + ", email=" + email + ", roles=" + roles + "]";
  }
  
}
