package com.data;

import java.util.List;


public class ParkingPlaceMongo {

  private Integer placeID;
  private List<Comment> comments;  
  private String ind;

  public ParkingPlaceMongo(Integer placeID, List<Comment> comments, String ind) {
    super();
    this.placeID = placeID;
    this.comments = comments;
    this.ind = ind;    
  }

  public synchronized String getInd() {
    return ind;
  }

  public synchronized void setInd(String ind) {
    this.ind = ind;
  }

  private Integer rating;

  public ParkingPlaceMongo(Integer placeID, List<Comment> comments) {
    super();
    this.placeID = placeID;
    this.comments = comments;
  }

  public ParkingPlaceMongo() {
    super();
  }

  public synchronized Integer getPlaceID() {
    return placeID;
  }

  public synchronized void setPlaceID(Integer placeID) {
    this.placeID = placeID;
  }

  public synchronized Integer getRating() {
    return rating;
  }

  public synchronized List<Comment> getComments() {
    return comments;
  }

  public synchronized void setComments(List<Comment> comments) {
    this.comments = comments;
  }

  public void addComment(Comment comment){
    this.comments.add(comment);
  }
  
  @Override
  public String toString() {
    return "ParkingPlaceMongo [placeID=" + placeID + ", comments=" + comments + "]";
  }

}
