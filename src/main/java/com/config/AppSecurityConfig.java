package com.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

  
  @Autowired
  @Qualifier("customUserDetailsService")
  UserDetailsService userDetailsService;
  
  
  @Autowired
  DataSource dataSource;
  
  @Autowired    // <- инжектим стандартный AuthenticationManager. Откуда он и где создался? - 2
  public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
      auth.userDetailsService(userDetailsService);           
      auth.authenticationProvider(authenticationProvider());  //определяем AuthenticationProvider см. ниже - 3
  }
  
  @Bean
  public DaoAuthenticationProvider authenticationProvider() { //определяем AuthenticationProvider
      DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
      authenticationProvider.setUserDetailsService(userDetailsService);
      authenticationProvider.setPasswordEncoder(passwordEncoder()); // AuthenticationProvider использует определенные UserDetailsService и UserDetails - 4
      return authenticationProvider;
  }
  
  @Bean
  public PasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
  }
  
  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.authorizeRequests()            //конфигурируем security interceptor - 1 - Тут же определены фильтры.
        
                   // ↓ SecuredObject           ↓ ConfigAttribute
        .antMatchers("/inside/**").access("hasRole('REGISTERED_USER')")    
        .antMatchers("/restws/**").access("hasRole('REGISTERED_USER')")     // FilterSecurityInterceptor
        .antMatchers("/confidential/**").access("hasRole('CHIEF_MANAGER')")
      .and()
        .formLogin()
        .defaultSuccessUrl("/inside/places", false)     // UsernamePasswordAuthenticationFilter
        .loginPage("/login")        
        .usernameParameter("login")
        .passwordParameter("password")        
        .permitAll()
      .and()
        .rememberMe()
        .rememberMeParameter("remember-me")
        .tokenRepository(persistentTokenRepository())   // RememberMeAuthenticationFilter
        .tokenValiditySeconds(86400);
  }
  
  @Bean
  public PersistentTokenRepository persistentTokenRepository() {
      JdbcTokenRepositoryImpl  tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
      tokenRepositoryImpl.setDataSource(dataSource);
      return tokenRepositoryImpl;
  }
  
}
