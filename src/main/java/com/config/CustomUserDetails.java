package com.config;

import java.sql.Date;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUserDetails extends User {

  private static final long serialVersionUID = 4463499826196661395L;
  
  public CustomUserDetails(String username, String password, boolean enabled,
      boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
      Collection<? extends GrantedAuthority> authorities) {
    super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,
        authorities);
    // TODO Auto-generated constructor stub
  }

  public CustomUserDetails(String username, String password,
      Collection<? extends GrantedAuthority> authorities) {
    super(username, password, authorities);
    // TODO Auto-generated constructor stub
  }


  public String firstname;
  public String email;
  public Date birthdate;

  public synchronized String getFirstname() {
    return firstname;
  }

  public synchronized void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public synchronized String getEmail() {
    return email;
  }

  public synchronized void setEmail(String email) {
    this.email = email;
  }

  public synchronized Date getBirthdate() {
    return birthdate;
  }

  public synchronized void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }

  
}
